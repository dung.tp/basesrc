package com.testapptest.test.view

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.testapptest.test.R
import com.testapptest.test.application.MyApp
import com.testapptest.test.base.view.BaseActivity
import com.testapptest.test.base.viewmodel.IBaseViewModel
import com.testapptest.test.customize.listener.ISingleClickListener
import com.testapptest.test.databinding.ActivityMainBinding
import com.testapptest.test.extension.addDivider
import com.testapptest.test.extension.dp
import com.testapptest.test.extension.getDefault
import com.testapptest.test.extension.initLinear
import com.testapptest.test.model.IapDetail
import com.testapptest.test.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    val mAdapter : BuySubAdapter by lazy {
        BuySubAdapter{item ->
            mViewModel.buyItem(this, item)
        }
    }

    override fun getLayoutRes(): Int = R.layout.activity_main
    override fun getModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun initView() {
        super.initView()
        dataBinding.apply {
            rc_view.apply {
                initLinear(RecyclerView.VERTICAL)
                adapter = mAdapter
                addDivider(3.dp)
            }
        }
    }

    override fun initViewModel() {
        super.initViewModel()

        mViewModel.apply {
//            mViewModel.initBilling(this@MainActivity)
            listItem.observe(this@MainActivity, Observer {
                 mAdapter.updateData(it)
                it.forEach { listItem ->
                    MyApp.instance.billingDataSource.isPurchased(listItem.name)
                        ?.observe(this@MainActivity, Observer { isPurchased ->
                            onShowErrorDialog("buy item:  ${listItem.name}  ${listItem.type} is $isPurchased")

                        })
                }

            })
        }
    }

    override fun onCheckConnectingService(onSuccess: (() -> Unit)?) {

    }
}