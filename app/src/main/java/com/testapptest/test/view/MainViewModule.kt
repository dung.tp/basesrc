package com.testapptest.test.view

import com.testapptest.test.di.scope.FragmentScoped
import com.testapptest.test.viewmodel.MainViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [MainViewModelModule::class])
abstract class MainViewModule {
//    @FragmentScoped
//    @ContributesAndroidInjector
//    internal abstract fun contributeHomeFragment(): HomeFragment
}