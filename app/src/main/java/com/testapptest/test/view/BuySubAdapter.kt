package com.testapptest.test.view

import com.testapptest.test.R
import com.testapptest.test.base.adapter.BaseBindingAdapter
import com.testapptest.test.customize.listener.ISingleClickListener
import com.testapptest.test.customize.listener.OnSingleClickListener
import com.testapptest.test.databinding.ViewBuySubBinding
import com.testapptest.test.model.IapDetail

class BuySubAdapter(val onItemBuy: (IapDetail) -> Unit) : BaseBindingAdapter<ViewBuySubBinding, IapDetail>() {
    override fun getLayoutId(viewType: Int) = R.layout.view_buy_sub

    override fun bindViewHolder(binding: ViewBuySubBinding, position: Int) {
        val data = list[position]
        binding.apply {
            iapDeatail = data
            viewParent.setOnClickListener{
                onItemBuy.invoke(data)
            }
        }
    }

}