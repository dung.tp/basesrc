package com.testapptest.test.model

class IapDetail(
    val name: String,
    val type: String,
    val value: Int
) {
}