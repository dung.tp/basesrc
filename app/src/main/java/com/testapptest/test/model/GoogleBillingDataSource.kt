package com.testapptest.test.model

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import androidx.lifecycle.*
import com.android.billingclient.api.*
import com.google.gson.Gson
import com.testapptest.test.utils.billing.ActiveMutableLiveData
import com.testapptest.test.utils.billing.Security
import timber.log.Timber
import kotlin.collections.set


/**
 *
 *   GoogleBillingDataSource.kt
 *
 *   Created by Dung.tp on 26/05/2021.
 *
 */
private const val RECONNECT_TIMER_START_MILLISECONDS = 1L * 1000L
private const val RECONNECT_TIMER_MAX_TIME_MILLISECONDS = 1000L * 60L * 15L // 15 minutes
private const val SKU_DETAILS_REQUERY_TIME = 1000L * 60L * 60L * 4L // 4 hours

class GoogleBillingDataSource(
    application: Application,
    knownInappSKUs: Array<String>?,
    knownSubscriptionSKUs: Array<String>?,
    autoConsumeSKUs: Array<String>?
) : LifecycleObserver, PurchasesUpdatedListener, BillingClientStateListener {

    // Billing client, connection, cached data
    private val billingClient: BillingClient

    // known SKUs (used to query sku data and validate responses)
    private val knownInappSKUs: List<String>
    private val knownSubscriptionSKUs: List<String>

    // SKUs to auto-consume
    private val knownAutoConsumeSKUs: MutableSet<String>

    // LiveData that is mostly maintained so it can be transformed into observables.
    private val skuStateMap: MutableMap<String, MutableLiveData<SkuState>> = HashMap()
    private val skuDetailsLiveDataMap: MutableMap<String, MutableLiveData<SkuDetails>> = HashMap()

    // Observables that are used to communicate state.
    private val purchaseConsumptionInProcess: MutableSet<Purchase> = HashSet()
    private val newPurchase: MutableLiveData<List<String>> = MutableLiveData()
    private val purchaseConsumed: MutableLiveData<List<String>> = MutableLiveData()

    private val billingFlowInProcess = MutableLiveData<Boolean>()

    // how long before the data source tries to reconnect to Google play
    private var reconnectMilliseconds: Long = RECONNECT_TIMER_START_MILLISECONDS

    // when was the last successful SkuDetailsResponse?
    private var skuDetailsResponseTime: Long = -SKU_DETAILS_REQUERY_TIME

    init {
        this.knownInappSKUs = if (knownInappSKUs == null) {
            ArrayList()
        } else {
            listOf(*knownInappSKUs)
        }

        this.knownSubscriptionSKUs = if (knownSubscriptionSKUs == null) {
            ArrayList()
        } else {
            listOf(*knownSubscriptionSKUs)
        }

        knownAutoConsumeSKUs = HashSet()
        if (autoConsumeSKUs != null) {
            knownAutoConsumeSKUs.addAll(listOf(*autoConsumeSKUs))
        }

        initializeLiveData()
        billingClient = BillingClient.newBuilder(application)
            .setListener(this)
            .enablePendingPurchases()
            .build()
        billingClient.startConnection(this)
    }

    private fun initializeLiveData() {
        addSkuLiveData(knownInappSKUs)
        addSkuLiveData(knownSubscriptionSKUs)
        billingFlowInProcess.postValue(false)
    }

    private fun addSkuLiveData(skuList: List<String>?) {
        for (sku in skuList!!) {
            val skuState = MutableLiveData<SkuState>()
            val details  =  ActiveMutableLiveData<SkuDetails>(){
                if (SystemClock.elapsedRealtime() - skuDetailsResponseTime> SKU_DETAILS_REQUERY_TIME) {
                    skuDetailsResponseTime = SystemClock.elapsedRealtime();
                    Timber.e("Skus not fresh, requerying");
                    querySkuDetailsAsync();
                }
            }
            skuStateMap[sku] = skuState
            skuDetailsLiveDataMap[sku] = details
        }
    }

    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {
        when (billingResult.responseCode) {
            BillingClient.BillingResponseCode.OK ->
                if (null != purchases) {
                    Timber.e("onPurchasesUpdated: BillingResult purchase $purchases")
                    processPurchaseList(purchases, null)
                    return
                } else {
                    Timber.e("onPurchasesUpdated: Null Purchase List Returned from OK response!")
                }
            BillingClient.BillingResponseCode.USER_CANCELED -> {
                Timber.e("onPurchasesUpdated: User canceled the purchase")
            }
            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> {
                Timber.e("onPurchasesUpdated: The user already owns this item")
            }
            BillingClient.BillingResponseCode.DEVELOPER_ERROR -> {
                Timber.e("onPurchasesUpdated: Developer error means that Google Play " +
                        "does not recognize the configuration. If you are just getting started, " +
                        "make sure you have configured the application correctly in the " +
                        "Google Play Console. The SKU product ID must match and the APK you " +
                        "are using must be signed with release keys.")
            }

            else -> Timber.e("onPurchasesUpdated: BillingResult ${billingResult.responseCode} ${billingResult.debugMessage}}")
        }
        billingFlowInProcess.postValue(false)
    }

    // Try to restart the connection on the next request to
    // Google Play by calling the startConnection() method.
    override fun onBillingServiceDisconnected() {
        retryBillingServiceConnectionWithExponentialBackoff()
    }

    override fun onBillingSetupFinished(billingResult: BillingResult) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage
        Timber.e("onBillingSetupFinished: $responseCode $debugMessage")
        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                // The billing client is ready. You can query purchases here.
                // This doesn't mean that your app is set up correctly in the console -- it just
                // means that you have a connection to the Billing service.
                reconnectMilliseconds = RECONNECT_TIMER_START_MILLISECONDS
                querySkuDetailsAsync()
                refreshPurchasesAsync();
            }
            else -> retryBillingServiceConnectionWithExponentialBackoff()
        }

    }

    private fun querySkuDetailsAsync() {
        if (!knownInappSKUs.isNullOrEmpty()) {
            val params = SkuDetailsParams.newBuilder()
                .setType(BillingClient.SkuType.INAPP)
                .setSkusList(knownInappSKUs)

            billingClient.querySkuDetailsAsync(
                params.build()
            ) { billingResult, skuDetailsList ->
                // Process the result.
                onSkuDetailsResponse(billingResult, skuDetailsList)
            }
        }

        if (!knownSubscriptionSKUs.isNullOrEmpty()) {
            val params = SkuDetailsParams.newBuilder()
                .setType(BillingClient.SkuType.SUBS)
                .setSkusList(knownSubscriptionSKUs)

            billingClient.querySkuDetailsAsync(
                params.build()
            ) { billingResult, skuDetailsList ->
                // Process the result.
                onSkuDetailsResponse(billingResult, skuDetailsList)
            }
        }
    }

    /**
     * Retries the billing service connection with exponential backoff, maxing out at the time
     * specified by RECONNECT_TIMER_MAX_TIME_MILLISECONDS.
     */
    private fun retryBillingServiceConnectionWithExponentialBackoff() {
        handler.postDelayed(
            { billingClient.startConnection(this) },reconnectMilliseconds
        )

        reconnectMilliseconds = Math.min(
            reconnectMilliseconds * 2,
            RECONNECT_TIMER_MAX_TIME_MILLISECONDS
        )
    }

    /*
        GPBL v4 now queries purchases asynchronously. This only gets active
        purchases.
     */
    private fun refreshPurchasesAsync() {
        billingClient.queryPurchasesAsync(
            BillingClient.SkuType.INAPP
        ) { billingResult, list ->
            if (billingResult.responseCode != BillingClient.BillingResponseCode.OK) {
                Timber.e("Problem getting purchases: " + billingResult.debugMessage)
            } else {
                processPurchaseList(list, knownInappSKUs)
            }
        }

        billingClient.queryPurchasesAsync(
            BillingClient.SkuType.SUBS
        ) { billingResult, list ->
            if (billingResult.responseCode != BillingClient.BillingResponseCode.OK) {
                Timber.e("Problem getting subscriptions: " + billingResult.debugMessage )
            } else {
                processPurchaseList(list, knownSubscriptionSKUs)
            }
        }
        Timber.e("Refreshing purchases started.")
    }

    private fun onSkuDetailsResponse(billingResult: BillingResult, skuDetailsList: List<SkuDetails>?) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage
        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                Timber.e("onSkuDetailsResponse: $responseCode $debugMessage")
                if (skuDetailsList == null || skuDetailsList.isEmpty()) {
                    val mes = "Found null or empty SkuDetails -> Check to see if the SKUs you requested are correctly published in the Google Play Console."
                    Timber.e("onSkuDetailsResponse: $mes")
                } else {
                    for (skuDetails in skuDetailsList) {
                        val sku = skuDetails.sku
                        Timber.e("onSkuDetailsResponse ${Gson().toJson(skuDetails)}")
                        val detailsMutableLiveData = skuDetailsLiveDataMap[sku]
                        if (null != detailsMutableLiveData) {
                            detailsMutableLiveData.postValue(skuDetails);
                        } else {
                            Timber.e("Unknown sku: " + sku);
                        }
                    }
                }
            }
            BillingClient.BillingResponseCode.SERVICE_DISCONNECTED,
            BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE,
            BillingClient.BillingResponseCode.BILLING_UNAVAILABLE,
            BillingClient.BillingResponseCode.ITEM_UNAVAILABLE,
            BillingClient.BillingResponseCode.DEVELOPER_ERROR,
            BillingClient.BillingResponseCode.ERROR ->
                Timber.e("onSkuDetailsResponse: $responseCode $debugMessage")
            BillingClient.BillingResponseCode.USER_CANCELED ->
                Timber.e("onSkuDetailsResponse: $responseCode $debugMessage")
            BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED,
            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED,
            BillingClient.BillingResponseCode.ITEM_NOT_OWNED ->
                Timber.e("onSkuDetailsResponse: $responseCode $debugMessage")
            else -> Timber.e("onSkuDetailsResponse: $responseCode $debugMessage")
        }

        if (responseCode == BillingClient.BillingResponseCode.OK) {
            skuDetailsResponseTime = SystemClock.elapsedRealtime();
        } else {
            skuDetailsResponseTime = -SKU_DETAILS_REQUERY_TIME;
        }
    }

    /**
     * Launch the billing flow. This will launch an external Activity for a result, so it requires
     * an Activity reference. For subscriptions, it supports upgrading from one SKU type to another
     * by passing in SKUs to be upgraded.
     *
     * @param activity active activity to launch our billing flow from
     * @param sku SKU (Product ID) to be purchased
     * @param upgradeSkusVarargs SKUs that the subscription can be upgraded from
     * @return true if launch is successful
     */
    fun launchBillingFlow(activity: Activity?, sku: String) {
        Timber.e("launchBillingFlow: ${skuDetailsLiveDataMap[sku]}")
        Timber.e("launchBillingFlow: ${skuDetailsLiveDataMap[sku]}")

        val skuDetails = skuDetailsLiveDataMap[sku]?.value
        if (null != skuDetails) {
            val billingFlowParamsBuilder = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)

            val billingResult = billingClient.launchBillingFlow(activity!!, billingFlowParamsBuilder.build()).responseCode
            if(billingResult==BillingClient.BillingResponseCode.OK){
                Timber.e("SkuDetails success for: $sku")
                billingFlowInProcess.postValue(true)
            } else {
                Timber.e("Billing failed $billingResult")
            }
        } else {
            Timber.e("SkuDetails not found for: $sku")
        }
    }

    private fun processPurchaseList(purchases: List<Purchase>?, skusToUpdate: List<String>?) {
        val updatedSkus = HashSet<String>()
        if (null != purchases) {
            for (purchase in purchases) {
                for (sku  in purchase.skus) {
                    val skuStateFlow = skuStateMap[sku]
                    if (null == skuStateFlow) {
                        Timber.e(
                            "processPurchaseList: Unknown SKU " + sku + ". Check to make " +
                                    "sure SKU matches SKUS in the Play developer console."
                        )
                        continue
                    }
                    updatedSkus.add(sku)
                }
                // Global check to make sure all purchases are signed correctly.
                // This check is best performed on your server.
                val purchaseState = purchase.purchaseState
                if (purchaseState == Purchase.PurchaseState.PURCHASED) {
                    if (!isSignatureValid(purchase)) {
                        Timber.e("processPurchaseList: Invalid signature. Check to make sure your public key is correct.")
                        continue
                    }

                    // only set the purchased state after we've validated the signature.
                    setSkuStateFromPurchase(purchase)
                    var isConsumable = false
                    for (sku in purchase.skus) {
                        if (knownAutoConsumeSKUs.contains(sku)) {
                            isConsumable = true
                        } else {
                            if (isConsumable) {
                                Timber.e("Purchase cannot contain a mixture of consumable and non-consumable items: " + purchase.skus.toString())
                                isConsumable = false
                                break
                            }
                        }
                    }

                    if (isConsumable) {
                        consumePurchase(purchase)
                    } else if (!purchase.isAcknowledged) {
                        // acknowledge everything --- new purchases are ones not yet acknowledged
                        billingClient.acknowledgePurchase(
                            AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.purchaseToken)
                                .build()
                        ) { billingResult ->
                            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                                // purchase acknowledged
                                for (sku in purchase.skus) {
                                    setSkuState(
                                        sku!!,
                                        SkuState.SKU_STATE_PURCHASED_AND_ACKNOWLEDGED
                                    )
                                }
                                newPurchase.postValue(purchase.skus)
                            }
                        }
                    }

                } else {
                    // make sure the state is set
                    setSkuStateFromPurchase(purchase)
                }
            }
        } else {
            Timber.e("Empty purchase list.")
        }
        // Clear purchase state of anything that didn't come with this purchase list if this is
        // part of a refresh.
        if (null != skusToUpdate) {
            for (sku in skusToUpdate) {
                if (!updatedSkus.contains(sku)) {
                    setSkuState(sku, SkuState.SKU_STATE_UNPURCHASED)
                }
            }
        }
    }

    /**
     * Since we (mostly) are getting sku states when we actually make a purchase or update
     * purchases, we keep some internal state when we do things like acknowledge or consume.
     * @param sku product ID to change the state of
     * @param newSkuState the new state of the sku.
     */
    private fun setSkuState(sku: String, newSkuState: SkuState) {
        val skuStateFlow = skuStateMap[sku]
        skuStateFlow?.postValue(newSkuState)
    }

    /**
     * Internal call only. Assumes that all signature checks have been completed and the purchase is
     * ready to be consumed. If the sku is already being consumed, does nothing.
     *
     * @param purchase purchase to consume
     */
    private fun consumePurchase(purchase: Purchase) {
        // weak check to make sure we're not already consuming the sku
        if (purchaseConsumptionInProcess.contains(purchase)) {
            // already consuming
            return
        }
        purchaseConsumptionInProcess.add(purchase)
        billingClient.consumeAsync(
            ConsumeParams.newBuilder()
                .setPurchaseToken(purchase.purchaseToken)
                .build()
        ) { billingResult: BillingResult, s: String? ->
            // ConsumeResponseListener
            purchaseConsumptionInProcess.remove(purchase)
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                Timber.e("Consumption successful. Delivering entitlement.")
                purchaseConsumed.postValue(purchase.skus)
                for (sku in purchase.skus) {
                    // Since we've consumed the purchase
                    setSkuState(sku, SkuState.SKU_STATE_UNPURCHASED)
                    // And this also qualifies as a new purchase
                }
                newPurchase.postValue(purchase.skus)
            } else {
                Timber.e("Error while consuming: " + billingResult.debugMessage)
            }
            Timber.e("End consumption flow.")
        }
    }

    /**
     * Calling this means that we have the most up-to-date information for a Sku in a purchase
     * object. This uses the purchase state (Pending, Unspecified, Purchased) along with the
     * acknowledged state.
     * @param purchase an up-to-date object to set the state for the Sku
     */
    private fun setSkuStateFromPurchase(purchase: Purchase) {
        for (purchaseSku in purchase.skus) {
            val skuStateFlow = skuStateMap[purchaseSku]
            if (null == skuStateFlow) {
                Timber.e("Unknown SKU $ purchaseSku Check to make sure SKU matches SKUS in the Play developer console."
                )
            } else {
                when (purchase.purchaseState) {
                    Purchase.PurchaseState.PENDING -> skuStateFlow.postValue(SkuState.SKU_STATE_PENDING)
                    Purchase.PurchaseState.UNSPECIFIED_STATE -> skuStateFlow.postValue(SkuState.SKU_STATE_UNPURCHASED)
                    Purchase.PurchaseState.PURCHASED -> if (purchase.isAcknowledged) {
                        skuStateFlow.postValue(SkuState.SKU_STATE_PURCHASED_AND_ACKNOWLEDGED)
                    } else {
                        skuStateFlow.postValue(SkuState.SKU_STATE_PURCHASED)
                    }
                    else -> Timber.e("Purchase in unknown state: " + purchase.purchaseState)
                }
            }
        }
    }

    /**
     * Ideally your implementation will comprise a secure server, rendering this check
     * unnecessary. @see [Security]
     */
    private fun isSignatureValid(purchase: Purchase): Boolean {
        return Security.verifyPurchase(purchase.originalJson, purchase.signature)
    }

    /**
     * It's recommended to requery purchases during onResume.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        Timber.e("ON_RESUME")
        // this just avoids an extra purchase refresh after we finish a billing flow
        billingFlowInProcess?.value?.let {
            if (!it) {
                if (billingClient.isReady) {
                    refreshPurchasesAsync();
                }
            }
        }

    }




    /**
     * This is a single live event that observes new purchases. These purchases can be the result of
     * a billing flow or from another source.
     *
     * @return LiveData that contains the sku of the new purchase.
     */
    fun observeNewPurchases(): LiveData<List<String>> {
        return newPurchase
    }

    /**
     * This is a single live event that observes consumed purchases from calling the consume
     * method.
     *
     * @return LiveData that contains the sku of the consumed purchase.
     */
    fun observeConsumedPurchases(): LiveData<List<String>> {
        return purchaseConsumed
    }

    /**
     * Returns whether or not the user has purchased a SKU. It does this by returning a
     * MediatorLiveData that returns true if the SKU is in the PURCHASED state and the Purchase has
     * been acknowledged.
     *
     * @return a LiveData that observes the SKUs purchase state
     */
    fun isPurchased(sku: String?): LiveData<Boolean?>? {
        val skuStateLiveData: LiveData<SkuState> = skuStateMap[sku]!!
        return skuStateLiveData.map { skuState -> skuState == SkuState.SKU_STATE_PURCHASED_AND_ACKNOWLEDGED }
    }

    private fun canPurchaseFromSkuDetailsAndPurchaseLiveData(
       result: MediatorLiveData<Boolean>,
       skuDetailsLiveData: LiveData<SkuDetails>,
       skuStateLiveData: LiveData<SkuState>
    ) {
        val skuState = skuStateLiveData!!.value
        if (null == skuDetailsLiveData.value) {
            result.setValue(false)
        } else {
            // this might be a transient state, but if we don't know about the purchase, we
            // typically can purchase. Not valid purchases can be purchased.
            result.setValue(
                null == skuState
                        || skuState === SkuState.SKU_STATE_UNPURCHASED
            )
        }
    }

    /**
     * Returns whether or not the user can purchase a SKU. It does this by returning a LiveData
     * transformation that returns true if the SKU is in the UNSPECIFIED state, as well as if we
     * have skuDetails for the SKU.
     *
     * @return a LiveData that observes the SKUs purchase state
     */
    fun canPurchase(sku: String?): LiveData<Boolean>? {
        val result = MediatorLiveData<Boolean>()
        val skuDetailsLiveData : LiveData<SkuDetails>  = skuDetailsLiveDataMap[sku]!!
        val skuStateLiveData: LiveData<SkuState> = skuStateMap[sku]!!
        assert(skuDetailsLiveData != null)
        // set initial state from LiveData values before observation callbacks.
        if (skuDetailsLiveData != null) {
            canPurchaseFromSkuDetailsAndPurchaseLiveData(result, skuDetailsLiveData, skuStateLiveData)
        }
        result.addSource(skuDetailsLiveData,
            Observer { skuDetails: SkuDetails? ->
                canPurchaseFromSkuDetailsAndPurchaseLiveData(
                    result, skuDetailsLiveData,
                    skuStateLiveData
                )
            }
        )
        result.addSource(skuStateLiveData,
            Observer {
                skuDetailsLiveData?.let { it1 ->
                    canPurchaseFromSkuDetailsAndPurchaseLiveData(
                        result, it1,
                        skuStateLiveData
                    )
                }
            }
        )
        return result
    }

    /**
     * The title of our SKU from SkuDetails.
     *
     * @param sku to get the title from
     * @return title of the requested SKU as an observable LiveData<String>
    </String> */
    fun getSkuTitle(sku: String?): LiveData<String> {
        val skuDetailsLiveData: LiveData<SkuDetails> = skuDetailsLiveDataMap[sku]!!
        return Transformations.map(
            skuDetailsLiveData
        ) { obj: SkuDetails -> obj.title }
    }

    // There's lots of information in SkuDetails, but our app only needs a few things, since our
    // goods never go on sale, have introductory pricing, etc.

    // There's lots of information in SkuDetails, but our app only needs a few things, since our
    // goods never go on sale, have introductory pricing, etc.
    fun getSkuPrice(sku: String?): LiveData<String> {
        val skuDetailsLiveData: LiveData<SkuDetails> = skuDetailsLiveDataMap[sku]!!
        return Transformations.map(
            skuDetailsLiveData
        ) { obj: SkuDetails -> obj.price }
    }

    fun getSkuDescription(sku: String?): LiveData<String> {
        val skuDetailsLiveData: LiveData<SkuDetails> = skuDetailsLiveDataMap[sku]!!
        return Transformations.map(
            skuDetailsLiveData
        ) { obj: SkuDetails -> obj.description }
    }


    private enum class SkuState {
        SKU_STATE_UNPURCHASED,
        SKU_STATE_PENDING,
        SKU_STATE_PURCHASED,
        SKU_STATE_PURCHASED_AND_ACKNOWLEDGED
    }

    companion object {

        @Volatile
        private var sInstance: GoogleBillingDataSource? = null
        private val handler = Handler(Looper.getMainLooper())

        fun getInstance(
            application: Application,
            knownInappSKUs: Array<String>?,
            knownSubscriptionSKUs: Array<String>?,
            autoConsumeSKUs: Array<String>?
        ) = sInstance ?: synchronized(this) {
            sInstance ?: GoogleBillingDataSource(
                application,
                knownInappSKUs,
                knownSubscriptionSKUs,
                autoConsumeSKUs
            )
                .also { sInstance = it }
        }
    }
}
