package com.testapptest.test.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.testapptest.test.BuildConfig
import com.testapptest.test.model.GoogleBillingDataSource
import com.testapptest.test.utils.Constants
import com.testapptest.test.utils.SharePreferencesUtils
import com.testapptest.test.utils.event.RxEvent
import com.testapptest.test.utils.event.SystemEvent
import com.testapptest.test.viewmodel.MainViewModel.Companion.AUTO_CONSUME_SKUS
import com.testapptest.test.viewmodel.MainViewModel.Companion.INAPP_SKUS
import com.testapptest.test.viewmodel.MainViewModel.Companion.SUBSCRIPTION_SKUS
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber
import javax.inject.Inject

class MyApp : DaggerApplication(), Application.ActivityLifecycleCallbacks {

    private var activityReferences = 0
    private var isActivityChangingConfigurations = false
    lateinit var billingDataSource : GoogleBillingDataSource

    init {
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

    override fun onCreate() {
        super.onCreate()
        // Set context for SharePreferencesUtils
        SharePreferencesUtils.context = this
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().build())

        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        registerActivityLifecycleCallbacks(this)

        billingDataSource = GoogleBillingDataSource.getInstance(
            instance,
            INAPP_SKUS,
            SUBSCRIPTION_SKUS,
            AUTO_CONSUME_SKUS
        )
    }




    override fun onActivityStarted(activity: Activity) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations && !activityVisible) {
//            App in background
            activityVisible = true
        }
    }


    override fun onActivityResumed(activity: Activity) {

    }

    override fun onActivityPaused(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {
        isActivityChangingConfigurations = activity.isChangingConfigurations
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            /*App in background*/
            activityVisible = false
        }
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {}

    override fun onActivityDestroyed(activity: Activity) {
        if (activityReferences == 0) {
        }
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

    companion object {
        lateinit var instance: MyApp

        private var activityVisible: Boolean = false
        fun isAppVisibility(): Boolean = activityVisible




    }
}