package com.testapptest.test.application

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.testapptest.test.ble.ServiceBuilderModule

import com.testapptest.test.data.network.NetworkModule
import com.testapptest.test.data.preference.ConfigurationPrefs
import com.testapptest.test.data.preference.IConfigurationPrefs
import com.testapptest.test.di.ActivityModule
import com.testapptest.test.di.ConfigureModule
import com.testapptest.test.di.DBModule
import com.testapptest.test.di.DialogModule
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule
import io.realm.Realm
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        NetworkModule::class,
        ActivityModule::class,
        DialogModule::class,
        DBModule::class,
        ConfigureModule::class,
        ServiceBuilderModule::class
    ]
)
class AppModule {

    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideConfigPrefs(context: Context): IConfigurationPrefs {
        return ConfigurationPrefs(context)
    }

    @Provides
    internal fun provideRealm(): Realm = Realm.getDefaultInstance()

    @Provides
    @Singleton
    fun provideRestAdapter(client: OkHttpClient, gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
    }
}