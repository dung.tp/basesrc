package com.testapptest.test.utils

object Constants {
    const val KEY_BEARER = "Bearer"
    const val PATTERN_PARSE_TIME_SERVER = "yyyy-MM-dd'T'HH:mm:ss"
    const val PATTERN_PARSE_TIME_SERVER1 = "yyyy-MM-dd'T'HH"
    const val PROFILE_TIME_FORMAT = "dd/MM/yyyy"
    const val CHART_TIME_FORMAT = "EEE dd/MM/yyyy"
    const val PROFILE_TIME_SERVER = "yyyy-MM-dd"
    const val KEY_PUT_OBJECT = "KEY_PUT_OBJECT"
    const val KEY_PUT_OBJECT_2 = "KEY_PUT_OBJECT_2"
    /*SOCKET*/
    const val KEY_ON_CHANGE_DATA = "overview_data_change"
    const val KEY_PAYMENT_SOCKET = "payment_status"
    const val KEY_EMIT_AUTHENTICATION = "authenticate"
    const val PATTERN_DATE_FULL_YEAR = "yyyyMMdd_HHMMss"
    const val EMPTY = ""
}