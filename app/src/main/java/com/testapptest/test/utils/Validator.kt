package com.testapptest.test.utils

import android.util.Log
import android.util.Patterns
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern


class Validator {

    companion object {

        // Default password validation messages
        private val PASSWORD_POLICY = """Password should be minimum 8 characters long,
            |should contain at least one capital letter,
            |at least one small letter,
            |at least one number and
            |at least one special character among ~!@#$%^&*()-_=+|[]{};:'\",<.>/?""".trimMargin()

        private val NAME_VALIDATION_MSG = "Enter a valid name"
        private val EMAIL_VALIDATION_MSG = "Enter a valid email address"
        private val PHONE_VALIDATION_MSG = "Enter a valid phone number"
        private val PASSWORD_MATCH_POLICY = "Your password and confirmation password do not match."

        /**
         * Checks if the email is valid.
         * @param data - can be EditText or String
         * @param updateUI - if true and if data is EditText, the function sets error to the EditText or its TextInputLayout
         * @return - true if the email is valid.
         */

        /**
         * Retrieve string data from the parameter.
         * @param data - can be EditText or String
         * @return - String extracted from EditText or data if its data type is Strin.
         */
        private fun getText(data: Any): String {
            var str = ""
            if (data is EditText) {
                str = data.text.toString()
            } else if (data is String) {
                str = data
            }
            return str
        }

        fun isValidEmail(data: Any, updateUI: Boolean = true): Boolean {
            val str = getText(data)
            val valid = Patterns.EMAIL_ADDRESS.matcher(str).matches()

            // Set error if required
            if (updateUI) {
                val error: String? = if (valid) null else EMAIL_VALIDATION_MSG
                setError(data, error)
            }

            return valid
        }

        fun isPhoneNumber(data: Any, updateUI: Boolean = true): Boolean {
            val str = getText(data)
            val valid = Patterns.PHONE.matcher(str).matches()

            // Set error if required
            if (updateUI) {
                val error: String? = if (valid) null else PHONE_VALIDATION_MSG
                setError(data, error)
            }

            return valid
        }

        /**
         * Checks if the password is valid as per the following password policy.
         * Password should be minimum minimum 8 characters long.
         * Password should contain at least one number.
         * Password should contain at least one capital letter.
         * Password should contain at least one small letter.
         * Password should contain at least one special character.
         * Allowed special characters: "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
         *
         * @param data - can be EditText or String
         * @param updateUI - if true and if data is EditText, the function sets error to the EditText or its TextInputLayout
         * @return - true if the password is valid as per the password policy.
         */
        fun isValidPassword(data: Any, updateUI: Boolean = true): Boolean {
            val str = if (data is EditText) {
                getText(data)
            } else {
                data.toString()
            }
            var valid = true

            // Password policy check
            // Password should be minimum minimum 8 characters long
            if (str.length < 8) {
                valid = false
            }
            /*// Password should contain at least one number
            var exp = ".*[0-9].*"
            var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
            var matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }

            // Password should contain at least one capital letter
            exp = ".*[A-Z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(str)
            if (!matcher.matches()) {
                valid = false
            }*/

            // Set error if required
            if (updateUI) {
                val error: String? = if (valid) null else PASSWORD_POLICY
                setError(data, error)
            }

            return valid
        }

        //check password and confirm password is match or not
        fun isMatchPassword(data: Any, dataConfirm: Any, updateUI: Boolean = true): Boolean {
            val str = getText(data)
            val strConfirm = getText(dataConfirm)
            var valid = true

            // Password policy check
            if (str.toString() != strConfirm.toString()) {
                valid = false
            }


            // Set error if required
            if (updateUI) {
                val error: String? = if (valid) null else PASSWORD_MATCH_POLICY
                setError(dataConfirm, error)
            }

            return valid
        }

        /**
         * Sets error on EditText or TextInputLayout of the EditText.
         * @param data - Should be EditText
         * @param error - Message to be shown as error, can be null if no error is to be set
         */
        private fun setError(data: Any, error: String?) {
            if (data is EditText) {
                if (data.parent.parent is TextInputLayout) {
                    (data.parent.parent as TextInputLayout).setError(error)
                } else {
                    data.setError(error)
                }
            }
        }

        fun isValidMultiEmail(data: Any, updateUI: Boolean = true): ArrayList<String>? {
            val str = getText(data)
            val list = getListEmail(str)

            var valid = true
            if(list.isNullOrEmpty()){
                valid = false
            } else {
                for(item in list){
                    if(!Patterns.EMAIL_ADDRESS.matcher(item).matches()){
                        valid = false
                        break
                    }
                }
            }
            Log.d("isValidMultiEmail", "list $list $valid")

            // Set error if required
            if (updateUI) {
                val error: String? = if (valid) null else EMAIL_VALIDATION_MSG
                setError(data, error)
                Log.d("isValidMultiEmail", "error $error ")
            }

            return (if(valid) getListEmail(str) else null)
        }

        private fun getListEmail(str: String): ArrayList<String>? {
            Log.d("getListEmail", "str $str")
            if(str.isNullOrEmpty()){
                return null
            }
            val emailSeparate =","
            var input = str.trim()
            var result = ArrayList<String>()
            while(input!=null && input.contains(emailSeparate)){
                val pos = input.indexOf(emailSeparate, 0, false)
                result.add(input.substring(0, pos-1))
                input = input.substring(pos+1)
            }
            result.add(input)
            Log.d("getListEmail", "list $result")
            return result
        }

    }

}