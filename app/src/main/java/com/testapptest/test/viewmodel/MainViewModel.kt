package com.testapptest.test.viewmodel

import android.app.Activity
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.billingclient.api.BillingClient
import com.testapptest.test.application.MyApp
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.extension.showDialogCustom
import com.testapptest.test.model.GoogleBillingDataSource
import com.testapptest.test.model.IapDetail
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val context: Context
): BaseViewModel(){
    private lateinit var dataSource : GoogleBillingDataSource
    val _listItem = MutableLiveData<MutableList<IapDetail>>()
    val listItem : LiveData<MutableList<IapDetail>>
        get() {
           return  _listItem
        }

    init {
        val list = mutableListOf(
            IapDetail(SKU_PREMIUM, BillingClient.SkuType.INAPP, 100000),
            IapDetail(SKU_CONSUME, BillingClient.SkuType.INAPP, 50000),
            IapDetail(SKU_INFINITE_GAS_MONTHLY, BillingClient.SkuType.SUBS, 20000)
        )
        _listItem.value = list
    }

//    fun initBilling(activity: Activity){
//        dataSource = GoogleBillingDataSource(activity, INAPP_SKUS, SUBSCRIPTION_SKUS, AUTO_CONSUME_SKUS)
//    }

    fun buyItem(activity: Activity, data: IapDetail) {
        if(!canPurchase(data)){
            onApiError("CAN NOT BUY - BYE BYE")
        } else {
            MyApp.instance.billingDataSource.launchBillingFlow(activity, data.name)
        }

    }
    fun canPurchase(data: IapDetail) : Boolean {
//        return MyApp.instance.billingDataSource.canPurchase(data.name)?.value!=null
////                && MyApp.instance.billingDataSource.canPurchase(data.name)?.value ==true

        return true
    }

    companion object {
        // Source for all constants
        const val CONSUME_TANK_MIN = 0
        const val CONSUME_TANK_MAX = 4
        const val CONSUME_TANK_INFINITE = 5

        // The following SKU strings must match the ones we have in the Google Play developer console.
        // SKUs for non-subscription purchases
        const val SKU_PREMIUM = "premium"
        const val SKU_CONSUME = "consume"

        // SKU for subscription purchases
        const val SKU_INFINITE_GAS_MONTHLY = "subscription_monthly"

        val INAPP_SKUS = arrayOf(SKU_PREMIUM, SKU_CONSUME)
        val SUBSCRIPTION_SKUS = arrayOf(
            SKU_INFINITE_GAS_MONTHLY
        )
        val AUTO_CONSUME_SKUS = arrayOf(SKU_CONSUME)
    }
}