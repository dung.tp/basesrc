package com.testapptest.test.base.viewmodel

import android.content.Context
import javax.inject.Inject

class EmptyViewModel @Inject constructor(context: Context) : BaseViewModel()