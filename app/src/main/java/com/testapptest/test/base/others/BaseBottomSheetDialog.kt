package com.testapptest.test.base.others

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.testapptest.test.R
import com.testapptest.test.data.preference.ConfigurationPrefs
import com.testapptest.test.data.preference.IConfigurationPrefs



abstract class BaseBottomSheetDialog<V : ViewDataBinding>(context: Context) :
        BottomSheetDialog(context, R.style.BottomSheetDialog), IBaseDialogView {

    protected lateinit var dataBinding: V

    protected abstract fun getLayoutId(): Int
    protected abstract fun init()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawableResource(android.R.color.transparent)

        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(context), getLayoutId(), null, false)
        setContentView(dataBinding.root)
        init()
    }

    override val viewContext: Context
        get() = context

    override val configPrefs: IConfigurationPrefs
        get() = ConfigurationPrefs(viewContext)

    override fun onClose() {
        dismiss()
    }
}