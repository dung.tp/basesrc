package com.testapptest.test.base.view

import android.content.Intent
import com.testapptest.test.R
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.extension.getString

interface IBaseActivity<T : BaseViewModel> : IBaseView<T> {
    fun onChangeStatusBarColor()
    fun onFullScreen()
    fun onCheckNewIntent(intent: Intent?)
    fun onAuthenticationHandler()
    fun getNavId(): Int?
    fun onShowErrorDialog(message: String)
    fun onCheckConnectingService(onSuccess: (() -> Unit)? = null)
    fun onShowSuccessDialog(
        title: String? = R.string.text_successfully.getString(),
        message: String,
        isHideGreenTick: Boolean = true,
        isCancelAble: Boolean = true,
        onClose: ((Boolean) -> Unit)? = null
    )
}