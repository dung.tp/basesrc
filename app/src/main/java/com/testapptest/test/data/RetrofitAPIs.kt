package com.testapptest.test.data

import io.reactivex.Completable
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface RetrofitAPIs {

    @Streaming
    @GET("")
    fun downloadFile(@Url fileUrl: String): Call<ResponseBody>

    @POST("")
    fun uploadFile( @Url url: String, @Body images: MultipartBody): Completable
}