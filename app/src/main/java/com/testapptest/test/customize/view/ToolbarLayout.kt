package com.testapptest.test.customize.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import com.testapptest.test.R
import com.testapptest.test.base.others.BaseCustomView
import com.testapptest.test.base.view.IBaseView
import com.testapptest.test.databinding.CustomToolbarBinding
import com.testapptest.test.extension.getString

class ToolbarLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : BaseCustomView(context, attrs, defStyle) {

    lateinit var binding: CustomToolbarBinding

    override fun initViewDataBinging(inflater: LayoutInflater): ViewDataBinding {
        binding = CustomToolbarBinding.inflate(inflater, this, true)
        return binding
    }

    override fun onCreatedView() {
        super.onCreatedView()
        tag = R.string.tag_tool_bar_view.getString()
    }

    fun initToolbar(iView: IBaseView<*>) {
        binding.iView = iView
    }

    companion object {
        @BindingAdapter("app:initToolbar")
        @JvmStatic
        fun ToolbarLayout.initToolbar(iView: IBaseView<*>) {
            binding.iView = iView
        }
    }
}