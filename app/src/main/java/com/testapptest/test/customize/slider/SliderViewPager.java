package com.testapptest.test.customize.slider;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.testapptest.test.R;
import com.testapptest.test.customize.slider.annotation.APageStyle;
import com.testapptest.test.customize.slider.annotation.Visibility;
import com.testapptest.test.customize.slider.constants.PageStyle;
import com.testapptest.test.customize.slider.manager.BannerManager;
import com.testapptest.test.customize.slider.manager.BannerOptions;
import com.testapptest.test.customize.slider.provider.ScrollDurationManger;
import com.testapptest.test.customize.slider.provider.ViewStyleSetter;
import com.testapptest.test.customize.slider.transform.ScaleInTransformer;
import com.testapptest.test.customize.slider.utils.BannerUtils;
import com.testapptest.test.customize.slider.transform.OverlapPageTransformer;

import java.util.ArrayList;
import java.util.List;

import static com.testapptest.test.customize.slider.BaseBannerAdapter.MAX_VALUE;
import static com.testapptest.test.customize.slider.transform.ScaleInTransformer.DEFAULT_MIN_SCALE;

public class SliderViewPager<T, V extends ViewDataBinding, VH extends BaseViewHolder<T, V>> extends RelativeLayout {

    private int currentPosition;

    private boolean isLooping;

    private OnPageClickListener mOnPageClickListener;

    private ViewPager2 mViewPager;

    private BannerManager mBannerManager;

    private Handler mHandler = new Handler();

    private BaseBannerAdapter<T, V, VH> mBannerPagerAdapter;

    private ViewPager2.OnPageChangeCallback onPageChangeCallback;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            handlePosition();
        }
    };

    private int startX, startY;

    private CompositePageTransformer mCompositePageTransformer;

    private MarginPageTransformer mMarginPageTransformer;

    private ViewPager2.PageTransformer mDefaultPageTransformer;

    private ViewPager2.OnPageChangeCallback mOnPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            int listSize = mBannerPagerAdapter.getListSize();
            int realPosition = BannerUtils.getRealPosition(isCanLoop(), position, listSize);
            if (listSize > 0) {
                if (onPageChangeCallback != null) {
                    onPageChangeCallback.onPageScrolled(realPosition, positionOffset, positionOffsetPixels);
                }
            }
        }

        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            int size = mBannerPagerAdapter.getListSize();
            currentPosition = BannerUtils.getRealPosition(isCanLoop(), position, size);
            if (size > 0 && isCanLoop() && position == 0 || position == MAX_VALUE - 1) {
                resetCurrentItem(currentPosition);
            }
            if (onPageChangeCallback != null) {
                onPageChangeCallback.onPageSelected(currentPosition);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            super.onPageScrollStateChanged(state);
            if (onPageChangeCallback != null) {
                onPageChangeCallback.onPageScrollStateChanged(state);
            }
        }
    };

    public SliderViewPager(Context context) {
        this(context, null);
    }

    public SliderViewPager(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SliderViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mCompositePageTransformer = new CompositePageTransformer();
        mBannerManager = new BannerManager();
        mBannerManager.initAttrs(context, attrs);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.bvp_layout, this);
        mViewPager = findViewById(R.id.vp_main);
        mViewPager.setPageTransformer(mCompositePageTransformer);
    }

    @Override
    protected void onDetachedFromWindow() {
        stopLoop();
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startLoop();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isLooping = true;
                stopLoop();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_OUTSIDE:
                isLooping = false;
                startLoop();
                break;
            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean canIntercept = mViewPager.isUserInputEnabled() || mBannerPagerAdapter != null && mBannerPagerAdapter.getData().size() <= 1;
        if (!canIntercept) {
            return super.onInterceptTouchEvent(ev);
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_MOVE:
                int endX = (int) ev.getX();
                int endY = (int) ev.getY();
                int disX = Math.abs(endX - startX);
                int disY = Math.abs(endY - startY);
                int orientation = mBannerManager.getBannerOptions().getOrientation();
                if (orientation == ViewPager2.ORIENTATION_VERTICAL) {
                    onVerticalActionMove(endY, disX, disY);
                } else if (orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                    onHorizontalActionMove(endX, disX, disY);
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                getParent().requestDisallowInterceptTouchEvent(false);
                break;
            case MotionEvent.ACTION_OUTSIDE:
            default:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    private void onVerticalActionMove(int endY, int disX, int disY) {
        if (disY > disX) {
            if (!isCanLoop()) {
                if (currentPosition == 0 && endY - startY > 0) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else if (currentPosition == getData().size() - 1 && endY - startY < 0) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
        } else if (disX > disY) {
            getParent().requestDisallowInterceptTouchEvent(false);
        }
    }

    private void onHorizontalActionMove(int endX, int disX, int disY) {
        if (disX > disY) {
            if (!isCanLoop()) {
                if (currentPosition == 0 && endX - startX > 0) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else if (currentPosition == getData().size() - 1 && endX - startX < 0) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
        } else if (disY > disX) {
            getParent().requestDisallowInterceptTouchEvent(false);
        }
    }

    private void handlePosition() {
        if (mBannerPagerAdapter.getListSize() > 1 && isAutoPlay()) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            mHandler.postDelayed(mRunnable, getInterval());
        }
    }

    private void initBannerData() {
        List<T> list = mBannerPagerAdapter.getData();
        if (list != null) {
            setupViewPager(list);
            initRoundCorner();
        }
    }

    private void initRoundCorner() {
        int roundCorner = mBannerManager.getBannerOptions().getRoundRectRadius();
        if (roundCorner > 0 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewStyleSetter viewStyleSetter = new ViewStyleSetter(this);
            viewStyleSetter.setRoundRect(roundCorner);
        }
    }

    public void setupViewPager(List<T> list) {
        if (mBannerPagerAdapter == null) {
            throw new NullPointerException("You must set adapter for BannerViewPager");
        }
        BannerOptions bannerOptions = mBannerManager.getBannerOptions();
        if (bannerOptions.getScrollDuration() != 0) {
            ScrollDurationManger.reflectLayoutManager(mViewPager, bannerOptions.getScrollDuration());
        }
        if (bannerOptions.getRightRevealWidth() != BannerOptions.DEFAULT_REVEAL_WIDTH || bannerOptions.getLeftRevealWidth() != BannerOptions.DEFAULT_REVEAL_WIDTH) {
            RecyclerView recyclerView = (RecyclerView) mViewPager.getChildAt(0);
            int orientation = bannerOptions.getOrientation();
            int padding2 = bannerOptions.getPageMargin() + bannerOptions.getRightRevealWidth();
            int padding1 = bannerOptions.getPageMargin() + bannerOptions.getLeftRevealWidth();
            if (orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                recyclerView.setPadding(padding1, 0, padding2, 0);
            } else if (orientation == ViewPager2.ORIENTATION_VERTICAL) {
                recyclerView.setPadding(0, padding1, 0, padding2);
            }
            recyclerView.setClipToPadding(false);
        }
        currentPosition = 0;
        mBannerPagerAdapter.setCanLoop(isCanLoop());
        mBannerPagerAdapter.setPageClickListener(mOnPageClickListener);
        mViewPager.setAdapter(mBannerPagerAdapter);
        if (list.size() > 1 && isCanLoop()) {
            mViewPager.setCurrentItem(MAX_VALUE / 2 - ((MAX_VALUE / 2) % list.size()) + 1, false);
        }
        mViewPager.unregisterOnPageChangeCallback(mOnPageChangeCallback);
        mViewPager.registerOnPageChangeCallback(mOnPageChangeCallback);
        mViewPager.setOrientation(bannerOptions.getOrientation());
        mViewPager.setOffscreenPageLimit(bannerOptions.getOffScreenPageLimit());
        initPageStyle();
        startLoop();
    }

    private void initPageStyle() {
        switch (mBannerManager.getBannerOptions().getPageStyle()) {
            case PageStyle.MULTI_PAGE_OVERLAP:
                setMultiPageStyle(true, mBannerManager.getBannerOptions().getPageScale());
                break;
            case PageStyle.MULTI_PAGE_SCALE:
                setMultiPageStyle(false, mBannerManager.getBannerOptions().getPageScale());
                break;
            default:
                break;
        }
    }

    private void setMultiPageStyle(boolean overlap, float scale) {
        if (mDefaultPageTransformer != null) {
            mCompositePageTransformer.removeTransformer(mDefaultPageTransformer);
        }
        if (overlap && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDefaultPageTransformer = new OverlapPageTransformer(mBannerManager.getBannerOptions().getOrientation(), scale, 0f, 1, 0);
        } else {
            mDefaultPageTransformer = new ScaleInTransformer(scale);
        }
        addPageTransformer(mDefaultPageTransformer);
    }


    private void resetCurrentItem(int item) {
        if (isCanLoop() && mBannerPagerAdapter.getListSize() > 1) {
            mViewPager.setCurrentItem(MAX_VALUE / 2 - ((MAX_VALUE / 2) % mBannerPagerAdapter.getListSize()) + 1 + item, false);
        } else {
            mViewPager.setCurrentItem(item, false);
        }
    }

    private int getInterval() {
        return mBannerManager.getBannerOptions().getInterval();
    }

    private boolean isAutoPlay() {
        return mBannerManager.getBannerOptions().isAutoPlay();
    }

    private boolean isCanLoop() {
        return mBannerManager.getBannerOptions().isCanLoop();
    }

    /**
     * @return BannerViewPager data set
     */
    public List<T> getData() {
        return mBannerPagerAdapter.getData();
    }

    /**
     * Start loop
     */
    public void startLoop() {
        if (!isLooping && isAutoPlay() && mBannerPagerAdapter != null &&
                mBannerPagerAdapter.getListSize() > 1) {
            mHandler.postDelayed(mRunnable, getInterval());
            isLooping = true;
        }
    }

    /**
     * stoop loop
     */
    public void stopLoop() {
        if (isLooping) {
            mHandler.removeCallbacks(mRunnable);
            isLooping = false;
        }
    }

    public SliderViewPager<T, V, VH> setAdapter(BaseBannerAdapter<T, V, VH> adapter) {
        this.mBannerPagerAdapter = adapter;
        return this;
    }

    public BaseBannerAdapter<T, V, VH> getAdapter() {
        return mBannerPagerAdapter;
    }

    /**
     * Set round rectangle effect for BannerViewPager.
     * <p>
     * Require SDK_INT>=LOLLIPOP(API 21)
     *
     * @param radius round radius
     */
    public SliderViewPager<T, V, VH> setRoundCorner(int radius) {
        mBannerManager.getBannerOptions().setRoundRectRadius(radius);
        return this;
    }

    /**
     * Set round rectangle effect for BannerViewPager.
     * <p>
     * Require SDK_INT>=LOLLIPOP(API 21)
     *
     * @param radius round radius
     */
    public SliderViewPager<T, V, VH> setRoundRect(int radius) {
        setRoundCorner(radius);
        return this;
    }

    /**
     * Enable/disable auto play
     *
     * @param autoPlay is enable auto play
     */
    public SliderViewPager<T, V, VH> setAutoPlay(boolean autoPlay) {
        mBannerManager.getBannerOptions().setAutoPlay(autoPlay);
        if (isAutoPlay()) {
            mBannerManager.getBannerOptions().setCanLoop(true);
        }
        return this;
    }

    /**
     * Enable/disable loop
     *
     * @param canLoop is can loop
     */
    public SliderViewPager<T, V, VH> setCanLoop(boolean canLoop) {
        mBannerManager.getBannerOptions().setCanLoop(canLoop);
        if (!canLoop) {
            mBannerManager.getBannerOptions().setAutoPlay(false);
        }
        return this;
    }

    /**
     * Set loop interval
     *
     * @param interval loop interval,unit is millisecond.
     */
    public SliderViewPager<T, V, VH> setInterval(int interval) {
        mBannerManager.getBannerOptions().setInterval(interval);
        return this;
    }

    /**
     * @param transformer PageTransformer that will modify each page's animation properties
     */
    public SliderViewPager<T, V, VH> setPageTransformer(@Nullable ViewPager2.PageTransformer transformer) {
        if (transformer != null) {
            mViewPager.setPageTransformer(transformer);
        }
        return this;
    }

    /**
     * @param transformer PageTransformer that will modify each page's animation properties
     */
    public SliderViewPager<T, V, VH> addPageTransformer(@Nullable ViewPager2.PageTransformer transformer) {
        if (transformer != null) {
            mCompositePageTransformer.addTransformer(transformer);
        }
        return this;
    }

    public void removeTransformer(@Nullable ViewPager2.PageTransformer transformer) {
        if (transformer != null) {
            mCompositePageTransformer.removeTransformer(transformer);
        }
    }

    public void removeDefaultPageTransformer() {
        if (mDefaultPageTransformer != null) {
            mCompositePageTransformer.removeTransformer(mDefaultPageTransformer);
        }
    }

    public void removeMarginPageTransformer() {
        if (mMarginPageTransformer != null) {
            mCompositePageTransformer.removeTransformer(mMarginPageTransformer);
        }
    }


    /**
     * set page margin
     *
     * @param pageMargin page margin
     */
    public SliderViewPager<T, V, VH> setPageMargin(int pageMargin) {
        mBannerManager.getBannerOptions().setPageMargin(pageMargin);
        if (mMarginPageTransformer != null) {
            mCompositePageTransformer.removeTransformer(mMarginPageTransformer);
        }
        mMarginPageTransformer = new MarginPageTransformer(pageMargin);
        mCompositePageTransformer.addTransformer(mMarginPageTransformer);
        return this;
    }


    /**
     * set item click listener
     *
     * @param onPageClickListener item click listener
     */
    public SliderViewPager<T, V, VH> setOnPageClickListener(OnPageClickListener onPageClickListener) {
        this.mOnPageClickListener = onPageClickListener;
        return this;
    }

    /**
     * Set page scroll duration
     *
     * @param scrollDuration page scroll duration
     */
    public SliderViewPager<T, V, VH> setScrollDuration(int scrollDuration) {
        mBannerManager.getBannerOptions().setScrollDuration(scrollDuration);
        return this;
    }

    public SliderViewPager<T, V, VH> setIndicatorVisibility(@Visibility int visibility) {
        mBannerManager.getBannerOptions().setIndicatorVisibility(visibility);
        return this;
    }

    /**
     * Create BannerViewPager with data.
     * If data has fetched when create BannerViewPager,you can call this method.
     */
    public void create(List<T> data) {
        if (mBannerPagerAdapter == null) {
            throw new NullPointerException("You must set adapter for BannerViewPager");
        }

        if (mBannerPagerAdapter.mList.size() == 0) {
            mBannerPagerAdapter.setData(data);
            initBannerData();
        } else {
            mBannerPagerAdapter.setData(data);
        }
    }

    public void create() {
        create(new ArrayList<T>());
    }

    /**
     * Sets the orientation of the ViewPager2.
     *
     * @param orientation {@link ViewPager2#ORIENTATION_HORIZONTAL} or
     *                    {@link ViewPager2#ORIENTATION_VERTICAL}
     */
    public SliderViewPager<T, V, VH> setOrientation(@ViewPager2.Orientation int orientation) {
        mBannerManager.getBannerOptions().setOrientation(orientation);
        return this;
    }

    /**
     * @return the currently selected page position.
     */
    public int getCurrentItem() {
        return currentPosition;
    }

    /**
     * Set the currently selected page. If the ViewPager has already been through its first
     * layout with its current adapter there will be a smooth animated transition between
     * the current item and the specified item.
     *
     * @param item Item index to select
     */
    public void setCurrentItem(int item) {
        if (isCanLoop() && mBannerPagerAdapter.getListSize() > 1) {
            int currentItem = mViewPager.getCurrentItem();
            int pageSize = mBannerPagerAdapter.getListSize();
            int realPosition = BannerUtils.getRealPosition(isCanLoop(), currentItem, mBannerPagerAdapter.getListSize());
            if (currentItem != item) {
                if (item == 0 && realPosition == pageSize - 1) {
                    mViewPager.setCurrentItem(currentItem + 1);
                } else if (realPosition == 0 && item == pageSize - 1) {
                    mViewPager.setCurrentItem(currentItem - 1);
                } else {
                    mViewPager.setCurrentItem(currentItem + (item - realPosition));
                }
                mViewPager.setCurrentItem(currentItem + (item - realPosition));
            }
        } else {
            mViewPager.setCurrentItem(item);
        }
    }

    /**
     * Set the currently selected page.
     *
     * @param item         Item index to select
     * @param smoothScroll True to smoothly scroll to the new item, false to transition immediately
     */
    public void setCurrentItem(int item, boolean smoothScroll) {
        if (isCanLoop() && mBannerPagerAdapter.getListSize() > 1) {
            int pageSize = mBannerPagerAdapter.getListSize();
            int currentItem = mViewPager.getCurrentItem();
            int realPosition = BannerUtils.getRealPosition(isCanLoop(), currentItem, pageSize);
            if (currentItem != item) {
                if (item == 0 && realPosition == pageSize - 1) {
                    mViewPager.setCurrentItem(currentItem + 1, smoothScroll);
                } else if (realPosition == 0 && item == pageSize - 1) {
                    mViewPager.setCurrentItem(currentItem - 1, smoothScroll);
                } else {
                    mViewPager.setCurrentItem(currentItem + (item - realPosition), smoothScroll);
                }
            }
        } else {
            mViewPager.setCurrentItem(item, smoothScroll);
        }
    }

    /**
     * Set Page Style for Banner
     * {@link PageStyle#NORMAL}
     * {@link PageStyle#MULTI_PAGE}
     *
     * @return BannerViewPager
     */
    public SliderViewPager<T, V, VH> setPageStyle(@APageStyle int pageStyle) {
        return setPageStyle(pageStyle, DEFAULT_MIN_SCALE);
    }

    public SliderViewPager<T, V, VH> setPageStyle(@APageStyle int pageStyle, float pageScale) {
        mBannerManager.getBannerOptions().setPageStyle(pageStyle);
        mBannerManager.getBannerOptions().setPageScale(pageScale);
        return this;
    }


    /**
     * @param revealWidth
     */
    public SliderViewPager<T, V, VH> setRevealWidth(int revealWidth) {
        setRevealWidth(revealWidth, revealWidth);
        return this;
    }

    /**
     * @param leftRevealWidth  left page item  reveal width
     * @param rightRevealWidth right page item reveal width
     */
    public SliderViewPager<T, V, VH> setRevealWidth(int leftRevealWidth, int rightRevealWidth) {
        mBannerManager.getBannerOptions().setRightRevealWidth(rightRevealWidth);
        mBannerManager.getBannerOptions().setLeftRevealWidth(leftRevealWidth);
        return this;
    }

    /**
     * Suggest to use default offScreenPageLimit.
     */
    public SliderViewPager<T, V, VH> setOffScreenPageLimit(int offScreenPageLimit) {
        mBannerManager.getBannerOptions().setOffScreenPageLimit(offScreenPageLimit);
        return this;
    }

    /**
     * Enable or disable user initiated scrolling
     */
    public SliderViewPager<T, V, VH> setUserInputEnabled(boolean userInputEnabled) {
        mBannerManager.getBannerOptions().setUserInputEnabled(userInputEnabled);
        mViewPager.setUserInputEnabled(userInputEnabled);
        return this;
    }

    public interface OnPageClickListener {
        void onPageClick(int position);
    }

    public SliderViewPager<T, V, VH> registerOnPageChangeCallback(ViewPager2.OnPageChangeCallback onPageChangeCallback) {
        this.onPageChangeCallback = onPageChangeCallback;
        return this;
    }

    /**
     * @deprecated user {@link #setUserInputEnabled(boolean)} instead.
     */
    @Deprecated
    public SliderViewPager<T, V, VH> disableTouchScroll(boolean disableTouchScroll) {
        mBannerManager.getBannerOptions().setUserInputEnabled(!disableTouchScroll);
        return this;
    }

}
