package com.testapptest.test.customize.dialog

import android.content.Context
import com.testapptest.test.extension.visible
import com.testapptest.test.R
import com.testapptest.test.base.others.BaseDialog
import com.testapptest.test.databinding.DialogSuccessLayoutBinding
import com.testapptest.test.extension.dp
import kotlinx.android.synthetic.main.dialog_success_layout.*

class SuccessDialog(
    context: Context,
    private val message: String?,
    private val title: String? = null,
    private val isHideGreenTick: Boolean? = false,
    private val isCancelAble: Boolean = true,
    private val onClosed: ((isOK: Boolean) -> Unit)? = null
) : BaseDialog<DialogSuccessLayoutBinding>(context) {
    override fun getLayoutId(): Int = R.layout.dialog_success_layout

    override fun init() {
        dataBinding.apply {
            tvTitle.apply {
                visible(title != null)
                text = title
            }

            tvMessage.text = message

            yes_button?.setOnClickListener {
                dismiss()
                onClosed?.invoke(true)
            }

            tvMessage.setPadding(0, if (isHideGreenTick == true) 12.dp else 0, 0, 0)

            imageDialog.visible(isHideGreenTick != true)

            if (!isCancelAble) {
                disableTouchOutside()
            }
        }
    }
}