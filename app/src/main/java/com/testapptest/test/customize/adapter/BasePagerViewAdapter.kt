package com.testapptest.test.customize.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.PagerAdapter



abstract class BasePagerViewAdapter<V : ViewDataBinding, T> : PagerAdapter() {
    var list: MutableList<T> = mutableListOf()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        DataBindingUtil.inflate<V>(
            LayoutInflater.from(container.context),
            getLayoutRes(),
            container,
            false
        ).apply {
            container.addView(root)
            initView(this, position)
            return root
        }
    }

    fun updateData(list: MutableList<T>) {
        this.list = list
        notifyDataSetChanged()
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    open fun initView(binding: V, position: Int) {}

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun getCount(): Int = list.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}