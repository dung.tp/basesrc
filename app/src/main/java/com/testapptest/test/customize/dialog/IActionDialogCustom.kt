package com.testapptest.test.customize.dialog

/**
 *
 *   IActionDialogCustom.kt
 *
 *   Created by ThangTX on 06/05/2021.
 *
 */
interface IActionDialogCustom {
    /**
     * Callback receiver action when user click button cancel
     */
    fun onCancelConfirmed()

    /**
     * Callback receiver action when user click button ok
     */
    fun onSuccessConfirmed()
}
